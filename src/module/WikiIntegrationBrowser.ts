import {
  Page,
  PageTreeItem,
  PageSearchResponse
} from '../interfaces/wikijs.interface';
import QueryHelper from './QueryHelper';
import { IQueryHelperOptions } from '../interfaces/queryHelper.interface';

export default class WikiIntegrationBrowser extends FormApplication {
  constructor(object = {}, options = { parent: null }) {
    super(object, options);

    //set up all the information needed by the queryhelper
    this.queryHelperOptions = {
      endPoint: game.settings.get('wikijs-integration', 'endPoint') || '',
      apiKey: game.settings.get('wikijs-integration', 'apiKey') || '',
      locale: game.settings.get('wikijs-integration', 'locale') || 'en'
    };
    this.hostAddress = game.settings.get('wikijs-integration', 'endPoint');
    this.queryHelperParamsValid = this._checkParamValidity();
    this.sanitizer = window['DOMPurify'];
  }
  queryHelperOptions: IQueryHelperOptions = null;

  parent = 0;
  hostAddress = '';
  tree: PageTreeItem[] = [];
  article: Page;
  queryHelperParamsValid: boolean;
  sanitizer: any;

  breadcrumbs: number[] = [];
  currentDepth = 0;

  //Search feature
  isSearch = false;
  searchResult: PageSearchResponse = null;
  searchQuery = '';

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: 'wikijs-integration-browser',
      title: 'Wiki.js Integration Browser',
      template: 'modules/wikijs-integration/templates/browser.html',
      classes: ['wikijs-integration', 'browser'],
      width: 900,
      height: 700,
      background: '#000',
      resizable: true,
      closeOnSubmit: false,
      submitOnClose: false,
      submitOnChange: false
    });
  }

  activateListeners(html: JQuery<HTMLElement>) {
    super.activateListeners(html);

    html.find('#getRootIndex').on('click', async (ev) => {
      if (!this.queryHelperParamsValid) {
        return;
      }
      await this._onGetRootIndex(ev);
      this.render(true);
    });

    html.find('.getPage').on('click', async (ev) => {
      if (!this.queryHelperParamsValid) {
        return;
      }
      await this._onGetPage(ev);
      this.render(true);
    });

    html.find('.getFolder').on('click', async (ev) => {
      if (!this.queryHelperParamsValid) {
        return;
      }
      await this._onGetFolder(ev);
      this.render(true);
    });

    html.find('.goBack').on('click', async (ev) => {
      this.currentDepth = Math.max(this.currentDepth - 1, 0);
      this.tree = await QueryHelper.getFolder(
        this.breadcrumbs[this.currentDepth] || 0,
        this.queryHelperOptions
      );
      this.breadcrumbs = this.breadcrumbs.slice(0, this.currentDepth + 1);
      this.render(true);
    });

    html.find('#importArticle').on('click', (ev) => this.importArticle());

    html
      .find('#wikijs-integration-browser #startSearch')
      .on('click', async (ev) => {
        const searchTerm = html.find('#searchInput').val() as string;
        //return early if there's no search
        if (!searchTerm) {
          ui.notifications.warn(game.i18n.localize('WIKI.SearchEmpty'));
          return;
        }
        this.searchQuery = searchTerm;
        this.isSearch = true;
        this.searchResult = await QueryHelper.searchForArticle(
          this.searchQuery,
          '',
          this.queryHelperOptions
        );
        this.render(true);
      });
    html.find('#resetSearch').on('click', async (ev) => {
      this.isSearch = false;
      this.searchQuery = '';
      await this._onGetRootIndex(ev);
      this.render(true);
    });
  }

  getData() {
    let data = super.getData();
    data.pages = this.tree;
    data.article = this.article;
    if (this.article) {
      data.articleRender = this.prepArticleForDisplay(this.article.render);
    }

    data.search = {
      searchResult: this.searchResult,
      isSearch: this.isSearch,
      query: this.searchQuery
    };

    data.displayImportButton = game.user.isGM && this.article;
    data.displayGoBack = this.currentDepth > 0;
    return data;
  }

  async importArticle() {
    let folder = await this._askForImportDestination();
    if (folder.cancelled) {
      console.log('Wiki.js Integration | User aborted article import');
      return;
    }
    let existingJournal = game.journal.find(
      (j: JournalEntry) =>
        j.getFlag('wikijs-integration', 'id') === this.article.id
    ) as JournalEntry;
    let preppedArticle = await this.prepArticleForImport(this.article.render);

    //If the article already exists.ask for overwrite/update
    if (existingJournal) {
      //check for newer
      let dialogContent = game.i18n.localize('WIKI.OverwriteText');
      if (
        new Date(this.article.updatedAt) >
        new Date(existingJournal.getFlag('wikijs-integration', 'modifiedAt'))
      ) {
        dialogContent = game.i18n.localize('WIKI.OverwriteNewerText');
      }
      Dialog.confirm({
        title: game.i18n.localize('WIKI.Overwrite'),
        content: dialogContent,
        yes: () => {
          existingJournal.update({
            content: preppedArticle
          });
        },
        no: () => {},
        defaultYes: false
      });
    } else {
      // If it doesn't then create a new one...
      let creationData = {
        name: this.article.title,
        content: preppedArticle,
        folder: folder.id,
        'flags.wikijs-integration': {
          id: this.article.id,
          modifiedAt: this.article.updatedAt
        }
      };
      JournalEntry.create(creationData, {
        renderSheet: true
      });
    }
  }

  prepArticleForDisplay(content: string) {
    const doc = new DOMParser().parseFromString(
      this.sanitizer.sanitize(content),
      'text/html'
    );
    //Remove TOC Anchors
    const tocAnchors = doc.querySelectorAll('a.toc-anchor');
    tocAnchors.forEach((v, k, p) => {
      v.remove();
    });

    //make images visible
    const images = doc.querySelectorAll('img');
    images.forEach((v, k, p) => {
      let src = v.getAttribute('src');
      if (src.startsWith('/')) {
        v.src = encodeURL(this.hostAddress.concat(src));
      }
    });

    //change relative links to point at the wiki
    const links = doc.querySelectorAll('a');
    links.forEach((v, k, p) => {
      let href = v.getAttribute('href');
      if (href.startsWith('/')) {
        v.href = encodeURL(this.hostAddress.concat(href));
      }
    });

    //format blockquotes
    const blockquotes = doc.querySelectorAll('blockquote');
    blockquotes.forEach((v, k, p) => {
      if (v.classList.length === 0) {
        v.classList.add('wikijs-bq');
      }
    });

    return this.sanitizer.sanitize(doc.body.innerHTML);
  }

  async prepArticleForImport(content: string) {
    const doc = new DOMParser().parseFromString(
      this.sanitizer.sanitize(content),
      'text/html'
    );
    //Remove TOC Anchors
    const tocAnchors = doc.querySelectorAll('a.toc-anchor');
    tocAnchors.forEach((v, k, p) => {
      v.remove();
    });

    //make images visible
    const images = doc.querySelectorAll('img');
    images.forEach((v, k, p) => {
      const src = v.getAttribute('src');
      if (src.startsWith('/')) {
        v.src = encodeURL(this.hostAddress.concat(src));
      }
    });

    //format blockquotes
    const blockquotes = doc.querySelectorAll('blockquote');
    blockquotes.forEach((v, k, p) => {
      if (v.classList.length === 0) {
        v.classList.add('wikijs-bq');
      }
    });

    //go through all the links, look the up on the wiki and the compare the article to saved journal entries
    const articles = doc.querySelectorAll('a');
    console.log(articles);
    for (let i = 0; i < articles.length; ++i) {
      const element = articles[i];
      let path = element.getAttribute('href');
      if (!path.startsWith('/')) {
        continue;
      }
      path = path.substring(1);
      const possibleHits = await QueryHelper.searchForArticle(
        '',
        path,
        this.queryHelperOptions
      );
      if (possibleHits.totalHits === 1) {
        const articleId = parseInt(possibleHits.results[0].id);
        const existingEntry: JournalEntry = game.journal.find(
          (j: JournalEntry) =>
            j.getFlag('wikijs-integration', 'id') === articleId
        );
        if (existingEntry) {
          const entityLink = document.createTextNode(
            `@JournalEntry[${existingEntry.id}]{${existingEntry.name}}`
          );
          element.parentElement.replaceChild(entityLink, element);
        }
      }
    }

    //change relative links to point at the wiki
    const links = doc.querySelectorAll('a');
    links.forEach((v, k, p) => {
      const href = v.getAttribute('href');
      if (href.startsWith('/')) {
        v.href = encodeURL(this.hostAddress.concat(href));
      }
    });

    return this.sanitizer.sanitize(doc.body.innerHTML);
  }

  private async _onGetRootIndex(ev) {
    this.tree = await QueryHelper.getRootIndex(this.queryHelperOptions);
    this.article = null;
    this.searchQuery = '';
    this.searchResult = null;
    this.breadcrumbs = [this.tree[0].parent];
  }

  private async _onGetPage(ev) {
    let pageId = ev.currentTarget.dataset.pageId;
    this.article = await QueryHelper.getPage(
      parseInt(pageId),
      this.queryHelperOptions
    );
  }

  private async _onGetFolder(ev) {
    const folderId = ev.currentTarget.dataset.folderId;
    //if the folder is also a page, load it and display it

    const page = this.tree.find((p) => p.id === parseInt(folderId));
    if (page?.pageId) {
      this.article = await QueryHelper.getPage(
        page.pageId,
        this.queryHelperOptions
      );
    }
    this.tree = await QueryHelper.getFolder(
      parseInt(folderId),
      this.queryHelperOptions
    );
    this.breadcrumbs.push(this.tree[0].parent);
    let depth = Math.max(this.tree[0].depth - 1, 0);
    this.currentDepth = depth;
  }

  private _checkParamValidity(): boolean {
    let retVal = true;
    if (!this.queryHelperOptions.endPoint) {
      ui.notifications.notify(game.i18n.localize('WIKI.HostEmpty'), 'error');
      retVal = false;
    }
    if (!this.queryHelperOptions.apiKey) {
      ui.notifications.notify(game.i18n.localize('WIKI.APIKeyEmpty'), 'error');
      retVal = false;
    }
    return retVal;
  }

  private async _askForImportDestination(): Promise<any> {
    const foldertree = SidebarDirectory.setupFolders(
      game.folders.filter((f) => f.data.type === 'JournalEntry'),
      [],
      'n'
    );

    const folders = [];

    for (const folder of foldertree.children) {
      folders.push({
        id: folder.id,
        name: folder.name
      });
      for (const firstChild of folder['children']) {
        folders.push({
          id: firstChild.id,
          name: `-> ${firstChild.name}`
        });
        for (const secondChild of firstChild['children']) {
          folders.push({
            id: secondChild.id,
            name: `-> -> ${secondChild.name}`
          });
        }
      }
    }

    const path = '/modules/wikijs-integration/templates/import-dialog.html';

    let retVal = {
      id: null,
      cancelled: true
    };

    const template = await renderTemplate(path, { folders });
    return new Promise((resolve) => {
      new Dialog({
        title: '',
        content: template,
        buttons: {
          ok: {
            label: game.i18n.localize('WIKI.Import'),
            icon: '<i class="fas fa-check"></i>',
            callback: (html: JQuery<HTMLElement>) => {
              retVal.id = html.find('#folder').val();
              retVal.cancelled = false;
            }
          },
          cancel: {
            label: game.i18n.localize('Cancel'),
            icon: '<i class="fas fa-times"></i>'
          }
        },
        default: 'ok',
        close: () => {
          resolve(retVal);
        }
      }).render(true);
    });
  }
}
