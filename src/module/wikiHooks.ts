import WikiIntegrationBrowser from './WikiIntegrationBrowser';

export default class WikiHooks {
  public static onPreUpdateJournalEntry(
    entity: JournalEntry,
    updateData: any,
    options: any,
    userId: string
  ) {
    if (entity.getFlag('wikijs-integration', 'modifiedAt')) {
      const timestamp = new Date().toJSON();
      updateData.flags = {
        'wikijs-integration': { modifiedAt: timestamp }
      };
    }
  }

  public static onRenderJournalDirectory(
    app: SidebarDirectory,
    html: JQuery,
    optios: any
  ) {
    if (
      !game.settings.get('wikijs-integration', 'allowUsers') &&
      !game.user.isGM
    ) {
      return;
    }

    const wikiBrowserButton = $(
      `<footer class="directory-footer">
        <button class="open-wiki-browser">
        <i class="fas fa-atlas"></i>
        ${game.i18n.localize('WIKI.OpenBrowser')}
        </button>
      </footer>`
    );
    html.find('header.directory-header').before(wikiBrowserButton);

    wikiBrowserButton.click((ev) => {
      new WikiIntegrationBrowser().render(true);
    });
  }
}
