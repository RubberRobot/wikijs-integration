export interface IQueryHelperOptions {
  endPoint: string;
  apiKey: string;
  locale?: string;
}
