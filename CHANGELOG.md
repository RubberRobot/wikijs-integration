# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/)

<!--
## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security
-->

## [v1.0.1]

### Added

- Added more links to module manifest

### Changed

- Updated Foundry compatability
- Updated DOMpurify to version 2.2.6
- Clarified how the endpoint URL should look like

### Fixed

- Fixed a css class scoping issue which would cause combatants in the combat tracker to disappear

## [v1.0.0]

### Added

- Ability to import Articles. Should you have previously have importet an article from your wiki and there's a link to it in the new article, that link will be replaced with an Entity link to the JournalEntry of the other article.
- Added validation on startup to make sure all the needed parameters are given.
- Added more language options
- Added more formatting
- Added ability to go back up through the tree
- Added module setting to set whether all users can see the Browser

## [0.1.0] Initial Release - **Alpha**

This is the initial release of the Wiki.js integration module. Add the host address, API Key and locale in the module settings. You can open the Wiki Browser via a button in the Journal list. See _README.md_ for future features.

Please remember that this is an alpha version and might contain bugs.
